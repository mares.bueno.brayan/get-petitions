// Routes.js - Módulo de rutas
const express = require('express');
const router = express.Router();

const arrayObjects = [{
  id:1,
  name: "Brayan",
  lastName: "Mares"
},
{
  id:2,
  name: "Jorge",
  lastName: "Perez"
},
{
  id:3,
  name: "Felipe",
  lastName: "Calderon"
},
{
  id:4,
  name: "Sergio",
  lastName: "Calderon"
},
{
  id:5,
  name: "Sergio",
  lastName: "Calderon"
}]

// Get mensajes
router.get('/', function (req, res) {
  res.json(arrayObjects);
});


module.exports = router;
